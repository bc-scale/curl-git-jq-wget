FROM alpine:3.15

RUN apk --update add curl git jq wget git-lfs gpg less openssh && \
    git lfs install && \
    rm -rf /var/lib/apt/lists/* && \
    rm /var/cache/apk/*
CMD ["--help"]
